import sys
from os import startfile, path
from PyQt5 import QtCore, QtWidgets, QtGui
from gui.Ui_login import Ui_RlxLoginUi
from db_process import DBProcess


class RallxLogin(QtWidgets.QMainWindow, Ui_RlxLoginUi):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.setMaximumSize(375,156)
        self.setMinimumSize(375,156)
        self.rallx_logo.setPixmap(QtGui.QPixmap('./gui/assets/logo/rlx.png'))
        # self.rallx_logo.setVisible(False)
        self.checkDB()
        self.loadFolders()
        self.faild = 0
        self.Interactions()
        self.installEventFilter(self)

    def loadFolders(self):
        db = DBProcess()
        data = db.tuple2List(db.listElement('name', 1))
        if data:
            self.fname.clear()
            self.fname.addItems(data)
        else:
            self.fname.clear()
            self.fname.addItem('Select Folder name')

    def checkDB(self):
        if not path.exists('data.db'):
            db = DBProcess()
            db.CreateTable(0)
            db.CreateTable(1)

    def Interactions(self):
# ------------------------ buttons ----------------------------------------------
        self.sfopen.clicked.connect(self.SfLogin)
        self.newf.clicked.connect(self.showRallxNewFolder)
        self.update_db.clicked.connect(self.updateDB)
# ------------------------- Menus ----------------------------------------------
        self.actionAdd_folder.triggered.connect(self.showRallxNewFolder)
        self.actionAdd_maze.triggered.connect(self.showRallxNewMaze)
        self.actionUpdate.triggered.connect(self.updateDB)
        self.actionModify_folder.triggered.connect(lambda: self.showRallxModifer(1))
        self.actionModify_maze.triggered.connect(lambda: self.showRallxModifer(0))

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.WindowActivate:
            self.loadFolders()
            self.pswd.setFocus()
        return super(RallxLogin, self).eventFilter(obj, event)

    def keyPressEvent(self, e):
        # super(RallxLogin,self).keyPressEvent(e)
        if e.key() == QtCore.Qt.Key_Return or e.key() == QtCore.Qt.Key_Enter:
            self.SfLogin()

        if e.key() == QtCore.Qt.Key_Escape:
            self.close()

    # def focusInEvent(self,e):
    #     print('hi')
    #     self.loadFolders()

    def showRallxNewFolder(self):
        from create_folder import RallxCreateFolder
        self.showCreateFolder = RallxCreateFolder()
        self.showCreateFolder.setWindowModality(QtCore.Qt.ApplicationModal)
        self.showCreateFolder.show()

    def showRallxNewMaze(self):
        from create_maze import RallxCreateMaze
        self.showCreateMaze = RallxCreateMaze()
        self.showCreateMaze.setWindowModality(QtCore.Qt.ApplicationModal)
        self.showCreateMaze.show()

    def showRallxModifer(self, tbl):
        from modifier import RallxModifier
        self.showModifier = RallxModifier(tbl)
        self.showModifier.setWindowModality(QtCore.Qt.ApplicationModal)
        self.showModifier.show()

    def updateDB(self):
        self.statusBar.setStyleSheet('')
        db = DBProcess()
        self.statusBar.showMessage('Updating Maze')
        db.syncDB(0)
        self.statusBar.showMessage('Maze updated', 3000)
        self.statusBar.showMessage('Updating Folders')
        db.syncDB(1)
        self.loadFolders()
        self.statusBar.showMessage('Folders updated', 3000)

    def SfLogin(self):
        name = self.fname.currentText()
        psswd = self.pswd.text()
        db = DBProcess()
        org_psswd = db.tuple2String(db.getElement('psswd', 'name', name, 1))
        loc = db.tuple2String(db.getElement('loc', 'name', name, 1))
        if loc:
            if psswd == org_psswd:
                self.statusBar.setStyleSheet('')
                self.pswd.setStyleSheet("")
                if path.exists(loc):
                    startfile(loc)
                    self.statusBar.showMessage('Opening...')
                    self.close()
                else:
                    self.statusBar.setStyleSheet('color: red;')
                    self.statusBar.showMessage(f'{name} is not found',3000)
                    self.statusBar.setStyleSheet('')
                    self.statusBar.showMessage(f'Press Sync to remove not existing folders',3000)
                # QtCore.QCoreApplication.instance().quit()
            else:
                self.pswd.setStyleSheet('border: 1px solid red;')
                self.statusBar.setStyleSheet('color: red;')
                self.statusBar.showMessage('INVALID PASSWORD')
                if self.faild == 3:
                    self.close()
                self.faild += 1
        else:
            self.statusBar.setStyleSheet('color: red;')
            self.statusBar.showMessage('No Folder Selected')


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    login = RallxLogin()
    login.show()
    login.pswd.setFocus()
    app.exec()
