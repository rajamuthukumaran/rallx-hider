import sqlite3
import os
from shutil import rmtree
from PyQt5 import QtCore, QtWidgets, QtGui
from gui.Ui_modifer import Ui_RlxModiferUi
from db_process import DBProcess

class RallxModifier(QtWidgets.QDialog, Ui_RlxModiferUi):

    def __init__(self, tbl, *args, **kwargs):
        super().__init__(*args,**kwargs)
        self.setupUi(self)
        self.setMaximumSize(504,158)
        self.setMinimumSize(504,158)
        self.tbl = tbl
        self.db = DBProcess()
        self.faild = 0
        self.access_state = False
        self.ops = ['Change Password','Delete']
        self.op_selector.clear()
        self.op_selector.addItems(self.ops)
        self.initView()
        self.Interactions()

    def Interactions(self):
        self.maze_btn.clicked.connect(self.loadMaze)
        self.folder_btn.clicked.connect(self.loadFolder)
        self.op_btn1.clicked.connect(lambda: self.opActions(1))
        self.op_btn2.clicked.connect(self.opActions)
        self.op_selector.currentTextChanged.connect(self.Operation)
        self.item_selector.currentTextChanged.connect(self.initAccess)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.close()
        if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
            if self.access_state:
                self.opActions()
            else:
                self.opActions(1)

    def initView(self):
        self.view_model = QtGui.QStandardItemModel()
        self.item_view.setModel(self.view_model)
        self.item_view.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.op_btn2.hide()
        self.txt_field1.setDisabled(True)
        self.txt_field2.setDisabled(True)
        self.op_selector.setDisabled(True)
        if self.tbl:
            self.folder_btn.setChecked(True)
            self.loadFolder()
        else:
            self.maze_btn.setChecked(True)
            self.loadMaze()
        self.psswd.setFocus()

    def initAccess(self):
        self.access_state = False
        self.view_model.clear()
        self.op_btn2.hide()
        self.psswd.setText('')
        self.psswd.setDisabled(False)
        self.uiRefresher()
        self.op_btn1.setDisabled(False)
        self.op_selector.setDisabled(True)

    def initOpView(self):
        self.uiRefresher()
        self.op_btn1.setDisabled(True)

    def uiRefresher(self):
        self.txt_field1.setDisabled(True)
        self.txt_field2.setDisabled(True)
        self.txt_field1.setText('')
        self.txt_field2.setText('')
        self.txt_field1.setPlaceholderText('')
        self.txt_field2.setPlaceholderText('')
        self.txt_field1.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.txt_field2.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.op_btn1.setText('Get Access')

    def loadMaze(self):
        data = self.db.tuple2List(self.db.listElement('name', 0))
        if data:
            self.item_selector.clear()
            self.item_selector.addItems(data)
        else:
            self.item_selector.clear()
            self.item_selector.addItem('No Maze')

    def loadFolder(self):
        data = self.db.tuple2List(self.db.listElement('name', 1))
        if data:
            self.item_selector.clear()
            self.item_selector.addItems(data)
        else:
            self.item_selector.clear()
            self.item_selector.addItem('No Folder')

    def loadMazeView(self):
        maze = self.item_selector.currentText()
        folders = self.db.tuple2List(self.db.listElement('name', 1, 'maze', maze))
        self.view_model.clear()
        if folders:
            print(folders)
            for i in folders:
                entry = QtGui.QStandardItem(i)
                self.view_model.appendRow(entry)


    def loadFolderView(self):
        loc = self.txt_field1.text()
        folders, files, size = self.getDirData(loc)
        data = [f'Folders: {folders}',f'Files: {files}',f'Size: {size}']
        self.view_model.clear()
        for i in data:
            entry = QtGui.QStandardItem(i)
            self.view_model.appendRow(entry)
        
        
    def getDirData(self, loc):
        tot_size = 0
        tot_files = 0
        tot_folders = 0
        for path, folder, files in os.walk(loc):
            for f in files:
                fpath = os.path.join(path, f)
                tot_size += os.path.getsize(fpath)
            tot_folders += len(folder)
            tot_files += len(files)
        return tot_folders, tot_files, tot_size
        

    def getAccess(self):
        self.psswd.setStyleSheet('')
        tbl = 1
        if self.maze_btn.isChecked():
            tbl = 0
        name = self.item_selector.currentText()
        chk = self.db.tuple2String(self.db.getElement('psswd', 'name', name, tbl))
        if self.psswd.text() == chk:
            self.giveAccess()
        else:
            self.psswd.setStyleSheet('border: 1px solid red;')
            if self.faild == 3:
                self.close()
            self.faild += 1

    def giveAccess(self):
        self.access_state = True
        if self.maze_btn.isChecked():
            self.loadMazeView()
        else:
            self.loadFolderView()
        self.psswd.setDisabled(True)
        self.op_selector.setDisabled(False)
        self.op_selector.setCurrentText(self.ops[0])
        self.initOpView()
        self.opChangePsswd()

    def Operation(self):
        self.initOpView()
        op = self.op_selector.currentText()
        if op == self.ops[0]:
            self.opChangePsswd()
        elif op == self.ops[1]:
            self.opDelete()

    def opActions(self,opcode=None):
        op = self.op_selector.currentText()
        if opcode:
            self.getAccess()
        else:
            if op == self.ops[0]:
                self.DBChangePsswd()
            elif op == self.ops[1]:
                self.DBDelete()
            

    # def opMove(self):
    #     opn = 1
    #     if self.maze_btn.isChecked():
    #         opn = 0
    #     name = self.item_selector.currentText()
    #     path = self.db.tuple2String(self.db.getElement('loc', 'name', name, opn))
    #     self.txt_field1.setText(path)
    #     self.txt_field2.setDisabled(False)
    #     self.op_btn1.setDisabled(False)
    #     self.op_btn1.setText('Move')
    #     self.op_btn2.setText('Browse')
    #     self.op_btn2.show()

    # def DBMove(self):
    #     print('Moving')

    def opChangePsswd(self):
        self.txt_field1.setDisabled(False)
        self.txt_field1.setFocus()
        self.txt_field2.setDisabled(False)
        self.txt_field1.setPlaceholderText('Enter the new password')
        self.txt_field2.setPlaceholderText('Confirm the password')
        self.txt_field1.setEchoMode(QtWidgets.QLineEdit.Password)
        self.txt_field2.setEchoMode(QtWidgets.QLineEdit.Password)
        self.op_btn2.show()
        self.op_btn2.setText('Change')

    def opDelete(self):
        self.op_btn2.setText('Delete')
        self.op_btn2.show()

    def DBChangePsswd(self):
        err1 = 'Please enter a password'
        err2 = 'Please confirm your password'
        if not self.dataCheck(1, err1, 1, err2):
            return
        new_pass = self.txt_field1.text()
        conf_pass = self.txt_field2.text()
        name = self.item_selector.currentText()
        tbl = 0
        if self.folder_btn.isChecked():
            tbl = 1
        if new_pass == conf_pass:
            self.txt_field1.setStyleSheet('border: 1px solid green; color: green;')
            self.txt_field2.setStyleSheet('border: 1px solid green; color: green;')
            self.db.modifyData('psswd', new_pass, 'name', name, tbl)
            self.close()
        else:
            self.txt_field2.setText('')
            self.txt_field2.setStyleSheet('border: 1px solid red; color: red;')
            self.txt_field2.setPlaceholderText("Password doesn't match")

    def DBDelete(self):
        name = self.item_selector.currentText()
        tbl = 0
        if self.folder_btn.isChecked():
            tbl = 1
        title = f'Delete {name}'
        msg1 = f'Do you wish to delete {name} from Storage?'
        msg2 = f'Please be advised that if you process all the data in {name} will be deleted. Do you wish to proceed?'
        msg3 = f'Do you wish to delete {name} from data without deleting it from storage?'
        if self.confMsg(title, msg1, 2):
            if self.confMsg(title, msg2, 2):
                loc = self.db.tuple2String(self.db.getElement('loc', 'name', name, tbl))
                rmtree(loc)
                self.db.syncDB(tbl)
                self.close()
        else:
            if self.confMsg(title, msg3, 2):
                self.db.deleteData(name, tbl)
                self.db.syncDB(tbl)
                self.close()

    def dataCheck(self, fd1, err1, fd2, err2):
        if fd1:
            self.txt_field1.setStyleSheet('')
            txt1 = self.txt_field1.text()
            if not txt1:
                self.txt_field1.setStyleSheet('border: 1px solid red; color: red;')
                self.txt_field1.setPlaceholderText(err1)
                return False
        if fd2:
            self.txt_field2.setStyleSheet('')
            txt2 = self.txt_field2.text()
            if not txt2:
                self.txt_field2.setStyleSheet('border: 1px solid red; color: red;')
                self.txt_field2.setPlaceholderText(err2)
                return False
        return True

    def confMsg(self, title, msg, btns):
        if btns == 1:
            resp = QtWidgets.QMessageBox.question(self, title, msg, QtWidgets.QMessageBox.Ok)
            return 1

        elif btns == 2:
            resp = QtWidgets.QMessageBox.question(self, title, msg, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
            if resp == QtWidgets.QMessageBox.Yes:
                return 1
            return 0

        elif btns == 3:
            resp = QtWidgets.QMessageBox.question(self, title, msg, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.Cancel)
            if resp == QtWidgets.QMessageBox.Yes:
                return 1
            if resp == QtWidgets.QMessageBox.No:
                return 0
            else:
                return 2



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = RallxModifier(1)
    window.show()
    app.exec()