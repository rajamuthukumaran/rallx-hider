import sys
from os import mkdir, path, system, startfile
from PyQt5 import QtCore, QtWidgets, QtGui
from gui.Ui_create_folder import Ui_RlxCreateFolder
from db_process import DBProcess


class RallxCreateFolder(QtWidgets.QMainWindow, Ui_RlxCreateFolder):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.setMinimumSize(428,164)
        self.setMaximumSize(428,164)
        self.faild = 0
        self.db = DBProcess()
        self.hidePath(7)
        self.loadMaze()
        self.Interactions()
        self.installEventFilter(self)

    def loadMaze(self):
        data = self.db.tuple2List(self.db.listElement('name', 0))
        if data:
            self.maze.clear()
            self.maze.addItem('Select Maze')
            self.maze.addItems(data)
        else:
            self.maze.clear()
            self.maze.addItem('Select Maze')

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.close()
        if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
            self.CreateFolder()

    def eventFilter(self, obj, event):
        if event.type() == QtCore.QEvent.WindowActivate:
            self.loadMaze()
        return super(RallxCreateFolder, self).eventFilter(obj, event)

    def Interactions(self):
        self.new_maze.clicked.connect(self.showRallxNewMaze)
        self.cancel.clicked.connect(self.close)
        self.start.clicked.connect(self.CreateFolder)
        self.maze.currentTextChanged.connect(self.MazePath)

    def showRallxNewMaze(self):
        from create_maze import RallxCreateMaze
        self.showCreateMaze = RallxCreateMaze()
        self.showCreateMaze.setWindowModality(QtCore.Qt.ApplicationModal)
        self.showCreateMaze.show()

    def MazePath(self):
        name = self.maze.currentText()
        if name == 'Select Maze' or name == '':
            self.hidePath(7)
            return
        size = self.db.tuple2String(
            self.db.getElement('size', 'name', name, 0))
        ptn = str(self.db.tuple2String(
            self.db.getElement('ptn', 'name', name, 0))).split(',')
        self.hidePath(7)
        self.showPath(size, ptn)

    def showPath(self, size, ptn):
        if 1 <= size:
            self.maze_1.show()
            self.maze_1.addItems(ptn)
        if 2 <= size:
            self.maze_2.show()
            self.maze_2.addItems(ptn)
        if 3 <= size:
            self.maze_3.show()
            self.maze_3.addItems(ptn)
        if 4 <= size:
            self.maze_4.show()
            self.maze_4.addItems(ptn)
        if 5 <= size:
            self.maze_5.show()
            self.maze_5.addItems(ptn)
        if 6 <= size:
            self.maze_6.show()
            self.maze_6.addItems(ptn)
        if 7 == size:
            self.maze_7.show()
            self.maze_7.addItems(ptn)

    def hidePath(self, size):
        if 1 <= size:
            self.maze_1.hide()
            self.maze_1.clear()
        if 2 <= size:
            self.maze_2.hide()
            self.maze_2.clear()
        if 3 <= size:
            self.maze_3.hide()
            self.maze_3.clear()
        if 4 <= size:
            self.maze_4.hide()
            self.maze_4.clear()
        if 5 <= size:
            self.maze_5.hide()
            self.maze_5.clear()
        if 6 <= size:
            self.maze_6.hide()
            self.maze_6.clear()
        if 7 == size:
            self.maze_7.hide()
            self.maze_7.clear()

    def getPath(self, size):
        path = ''
        if 1 <= size:
            path += '\\'
            path += self.maze_1.currentText()
        if 2 <= size:
            path += '\\'
            path += self.maze_2.currentText()
        if 3 <= size:
            path += '\\'
            path += self.maze_3.currentText()
        if 4 <= size:
            path += '\\'
            path += self.maze_4.currentText()
        if 5 <= size:
            path += '\\'
            path += self.maze_5.currentText()
        if 6 <= size:
            path += '\\'
            path += self.maze_6.currentText()
        if 7 == size:
            path += '\\'
            path += self.maze_7.currentText()
        return path

    def CreateFolder(self):
        self.s_folder_name.setStyleSheet('')
        self.maze.setStyleSheet('')
        self.statusbar.setStyleSheet('')

        name = self.s_folder_name.text()
        if name == '':
            self.s_folder_name.setStyleSheet('border: 1px solid red;')
            self.statusbar.setStyleSheet('color: red;')
            self.statusbar.showMessage('Enter the folder name')
            return

        psswd = self.pswrd.text()
        maze_psswd = self.maze_psswd.text()

        maze_name = self.maze.currentText()
        if maze_name == 'Select Maze':
            self.maze.setStyleSheet('border: 1px solid red;')
            self.statusbar.setStyleSheet('color: red;')
            self.statusbar.showMessage('Select a maze')
            return

        org_maze_psswd = self.db.tuple2String(
            self.db.getElement('psswd', 'name', maze_name, 0))
        maze_loc = self.db.tuple2String(
            self.db.getElement('loc', 'name', maze_name, 0))
        maze_size = self.db.tuple2String(
            self.db.getElement('size', 'name', maze_name, 0))
        maze_ptn = self.getPath(maze_size)
        loc = maze_loc+maze_ptn
        folder = loc+'\\'+name

        if maze_psswd == org_maze_psswd:
            self.maze_psswd.setStyleSheet('')
            self.statusbar.setStyleSheet('')
            self.statusbar.showMessage("Creating new folder...")

            if not self.db.AddFolder(name, psswd, maze_name, folder):
                self.statusbar.setStyleSheet('color: red;')
                self.statusbar.showMessage('Another Folder have this name')
                return

            if not path.exists(folder):
                mkdir(folder)
            system(f"attrib +s +h {folder}")
            if self.WantToCopy():
                startfile(folder)
            self.close()

        else:
            self.maze_psswd.setStyleSheet('border: 1px solid red;')
            self.statusbar.setStyleSheet('color: red;')
            self.statusbar.showMessage('INVALID PASSWORD')
            if self.faild == 3:
                self.close()
            self.faild += 1

    def WantToCopy(self):
        title = f'Add files to {self.s_folder_name.text()}'
        msg = 'Do you want to copy files to the new folder?'
        resp = QtWidgets.QMessageBox.question(self, title, msg, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)
        if resp == QtWidgets.QMessageBox.Yes:
            return True
        return False


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = RallxCreateFolder()
    window.show()
    app.exec()
