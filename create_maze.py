import sys
import os
from PyQt5 import QtCore, QtWidgets, QtGui
from gui.Ui_create_maze import Ui_RlxCreateMaze
from db_process import DBProcess


class RallxCreateMaze(QtWidgets.QDialog, Ui_RlxCreateMaze):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.setMaximumSize(408,184)
        self.setMinimumSize(408,184)
        self.Interactions()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.close()
        if event.key() == QtCore.Qt.Key_Enter or event.key() == QtCore.Qt.Key_Return:
            self.CreateMaze()

    def Interactions(self):
        self.maze_open.clicked.connect(self.openFolder)
        self.start.clicked.connect(self.CreateMaze)

    def openFolder(self):
        folder = QtWidgets.QFileDialog.getExistingDirectory(
            self, 'Select the folder')
        self.maze_folder.setText(folder)

    def CreateMaze(self):
        self.maze_name.setStyleSheet('')
        self.err_ptn.setText('')

        maze_pattern = self.maze_ptn.text()
        folder = self.maze_folder.text()
        name = self.maze_name.text()
        psswd = self.maze_psswd.text()
        ptn = str(maze_pattern).split(',')
        size = self.maze_size.text()
        db = DBProcess()

        if not self.debugMaze(folder, name, ptn, size):
            return

        loc = folder+'\\'+name
        if not db.AddMaze(name, psswd, loc, maze_pattern, size):
            self.err_ptn.setStyleSheet('color: red;')
            self.err_ptn.setText('Another Maze have this name')
            return

        if not os.path.exists(loc):
            os.makedirs(loc)
        os.system(f"attrib +s +h {loc}")

        self.length = len(loc.split('\\')) + int(size)
        self.createPattern(loc, ptn)
        self.err_ptn.setStyleSheet('border: 1px solid green;')
        self.err_ptn.setText(f"{name} created Successfully")
        self.close()

    def createPattern(self, loc, ptn):
        path_len = len(loc.split('\\'))
        if path_len == self.length:
            return
        for i in ptn:
            loc_path = (loc+'\\'+i)
            os.mkdir(loc_path)
            self.createPattern(loc_path, ptn)

    def debugMaze(self, folder, name, ptn, size):
        self.maze_ptn.setStyleSheet('')
        self.maze_size.setStyleSheet('')
        self.maze_folder.setStyleSheet('')
        self.maze_name.setStyleSheet('')
        self.err_ptn.setText('')

        if folder == '':
            self.maze_folder.setStyleSheet('border: 1px solid red;')
            return False

        if name == '':
            self.maze_name.setStyleSheet('border: 1px solid red;')
            return False

        if len(ptn) < 2:
            self.maze_ptn.setStyleSheet('border: 1px solid red;')
            self.err_ptn.setStyleSheet('color: red;')
            self.err_ptn.setText('Should have alteast 2 pattern')
            return False

        if size == '' or int(size) < 2:
            self.maze_size.setStyleSheet('border: 1px solid red;')
            self.err_ptn.setStyleSheet('color: red;')
            self.err_ptn.setText('Size should be more than 1')
            return False

        if size == '' or int(size) > 7:
            self.maze_size.setStyleSheet('border: 1px solid red;')
            self.err_ptn.setStyleSheet('color: red;')
            self.err_ptn.setText('Size should not be more than 7')
            return False

        return True


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = RallxCreateMaze()
    window.show()
    app.exec()
