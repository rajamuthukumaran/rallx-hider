import sqlite3
from os import path


class DBProcess:

    def typeToTable(self, tbl):
        if tbl == 0:
            return 'rallx_hider_maze'
        else:
            return 'rallx_hider_folder'

    def isEmpty(self, data):
        if data:
            return False
        return True

    def listElement(self, element, tbl, frm=None, condition=None):
        table = self.typeToTable(tbl)
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        if not condition:
            c.execute(f"SELECT {element} FROM {table}")
        else:
            c.execute(f"SELECT {element} FROM {table} where {frm}= :condition", {'condition': condition})
        data = c.fetchall()
        c.close()
        return data

    def tuple2List(self, data):
        arr = []
        if self.isEmpty(data):
            return ''
        for i in data:
            for k in i:
                arr.append(k)
        return arr

    def tuple2String(self, data):
        if self.isEmpty(data):
            return ''
        return data[0]

    def getElement(self, element, frm, value, tbl):
        table = self.typeToTable(tbl)
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(f"SELECT {element} FROM {table} WHERE {frm}= :value", {
                  'value': value})
        data = c.fetchone()
        c.close()
        return data

    def AddFolder(self, name, psswd, maze, loc):
        check = self.getElement('loc', 'name', name, 1)
        if check:
            return False
        table = self.typeToTable(1)
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(f"INSERT INTO {table} VALUES ( :name, :psswd, :maze, :loc )", {
                  'name': name, 'psswd': psswd, 'maze': maze, 'loc': loc})
        conn.commit()
        c.close()
        return True

    def AddMaze(self, name, psswd, loc, ptn, size):
        check = self.getElement('loc', 'name', name, 0)
        if check:
            return False
        table = self.typeToTable(0)
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(f"INSERT INTO {table} VALUES (:name, :psswd, :loc, :ptn, :size)", {
                  'name': name, 'psswd': psswd, 'loc': loc, 'ptn': ptn, 'size': size})
        conn.commit()
        c.close()
        return True

    def tableAttributes(self, tbl):
        if tbl == 0:
            return 'name TEXT NOT NULL UNIQUE PRIMARY KEY, psswd TEXT, loc TEXT NOT NULL, ptn TEXT NOT NULL, size INTEGER NOT NULL'
        else:
            return 'name TEXT NOT NULL UNIQUE PRIMARY KEY, psswd TEXT, maze TEXT NOT NULL, loc TEXT NOT NULL'

    def CreateTable(self, tbl):
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(f"CREATE TABLE {self.typeToTable(tbl)} ( {self.tableAttributes(tbl)} )")
        conn.commit()
        c.close()

    def DeleteTable(self, tbl):
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(
            f"CREATE TABLE {self.typeToTable(tbl)} ( {self.tableAttributes(tbl)} )")
        conn.commit()
        c.close()

    def syncDB(self, tbl):
        table = self.typeToTable(tbl)
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(f"select name,loc from {table}")
        data = c.fetchall()
        for i in data:
            print(i)
            if not path.exists(i[1]):
                print(i[0])
                self.deleteData(i[0], tbl)
        c.close()

    def modifyData(self, element, value, frm, condition, tbl):
        table = self.typeToTable(tbl)
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(f"UPDATE {table} SET {element} = :value WHERE {frm} = :condition", {'value': value, 'condition': condition})
        conn.commit()
        c.close()

    def deleteData(self, key, tbl):
        table = self.typeToTable(tbl)
        conn = sqlite3.connect('data.db')
        c = conn.cursor()
        c.execute(f"DELETE FROM {table} WHERE name= :key", {'key': key})
        conn.commit()
        c.close()


if __name__ == "__main__":
    db = DBProcess()
    # db.AddMaze('Official', 'a,b,c,d', 4, 'grand', r'g:\Downloads')
    # db.AddFolder('Personal', 'grand', r'g:\Downloads')
    # db.AddFolder('Official', 'grand', r'g:\Downloads')
    db.CreateTable(0)
    db.CreateTable(1)
    # db.CreateFolderTable(0)
    # print(db.listElement('name', 1))
